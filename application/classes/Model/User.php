<?php

class Model_User extends ORM {

    protected $_table_name = 'users';

    protected $_has_many = [
        'accounts'  => [
             'model'       => 'Account',
             'foreign_key' => 'user_id',
        ],
    ];

    public function rules()
    {
        return [
            'usr_name' => [
                ['not_empty'],
            ],
            'usr_email' => [
                ['not_empty'],
            ],
            'usr_address' => [
                ['not_empty'],
            ],
        ];
    }
}

<?php

class Controller_Index extends Controller_Template {

        public $template = 'template';

	public function action_index()
	{
                $users = ORM::factory('User')->find_all();

                $this->template->title = 'Main page';
                $this->template->content = View::factory('index', [
                        'users' => $users
                ]);
	}
}

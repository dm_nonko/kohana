<?php

class Controller_User extends Controller_Template {

    public function action_add()
    {
       if (Request::initial()->is_ajax()) {
           $data = $this->request->post('user') ?? [];
           try {
               ORM::factory('User')->values($data)->create();
           } catch (ORM_Validation_Exception $e) {
               $response = [];
               foreach ($e->errors() as $field => $errors) {
                   $response[$field] = $errors[0];
               }
               print_r($response);
           }
           $this->auto_render = false;
        }
    }
}

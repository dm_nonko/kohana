<?php

class Controller_Account extends Controller_Template {

        public function action_add()
        {
               if (Request::initial()->is_ajax()) {
                       $data = $this->request->post('account') ?? [];
                       $data['added'] = date("Y-m-d H:i:s");
                       ORM::factory('Account')->values($data)->create();
                       $this->auto_render = false;
               }
        }
}

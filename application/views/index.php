<h2>Create user</h2>
<?= Form::open('javascript://', [
    'method' => 'get',
    'id'     => 'user_form',
]); ?>

    <div class="row">
        <div class="col-md-3 form-group">
            <?= Form::input('user[usr_name]', '', [
                'class'       => 'form-control',
                'id'          => 'user-usr_name',
                'placeholder' => 'Enter name',
            ]); ?>
        </div>

        <div class="col-md-3 form-group">
            <?= Form::input('user[usr_email]', '', [
                'class'       => 'form-control',
                'id'          => 'user-usr_email',
                'placeholder' => 'Enter email',
            ]); ?>
       </div>

        <div class="col-md-3 form-group">
            <?= Form::input('user[usr_address]', '', [
                'class'       => 'form-control',
                'id'          => 'user-usr_address',
                'placeholder' => 'Enter address',
            ]); ?>
        </div>

        <div class="col-md-3">
            <?= Form::button('user_save', 'save', [
                'type'  => 'submit',
                'class' => 'btn btn-success',
            ]); ?>
        </div>
    </div>

<?= Form::close(); ?>

<hr />

<h2>Create account</h2>
<?= Form::open('javascript://', [
    'method' => 'get',
    'id'     => 'account_form',
]); ?>

    <div class="row">
        <div class="col-md-3 form-group">
            <?= Form::input('account[account]', '', [
                'class'       => 'form-control',
                'id'          => 'account-account',
                'placeholder' => 'Enter account',
            ]); ?>
        </div>

        <div class="col-md-3 form-group">
            <?php $usersList = array_combine(array_column($users->as_array(), 'id'),
                 array_column($users->as_array(), 'usr_name')); ?>
            <?= Form::select('account[user_id]', $usersList, array_keys($usersList)[0] ?? null, [
                'class' => 'form-control',
                'id'    => 'account-user_id',
            ]); ?>
        </div>

        <div class="col-md-3">
            <?= Form::button('account_save', 'save', [
                'type'  => 'submit',
                'class' => 'btn btn-success',
            ]); ?>
        </div>
    </div>

<?= Form::close(); ?>

<hr />

<table class="table">
    <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Accounts</th>
        <th>Date added</th>
    </thead>
    <tbody>
        <?php foreach ($users as $user): ?>
            <?php if ($user->accounts->find()->loaded()): ?>
                <?php foreach ($user->accounts->find_all() as $account): ?>
                <tr>
                    <td><?= $user->usr_name ?></td>
                    <td><?= $user->usr_email ?></td>
                    <td><?= $user->usr_address ?></td>
                    <td><?= $account->account ?></td>
                    <td><?= date("Y-m-d", strtotime($account->added)) ?></td>
                </tr>
                <?php endforeach ?>
            <?php else: ?>
                <tr>
                    <td><?= $user->usr_name ?></td>
                    <td><?= $user->usr_email ?></td>
                    <td><?= $user->usr_address ?></td>
                    <td>---</td>
                    <td>---</td>
                </tr>
            <?php endif ?>
        <?php endforeach ?>
    </tbody>
</table>

$(document).ready(function(e){
    $("#user_form").submit(function(event){
        $.ajax({
            url:  '/user/add',
            type: 'post',
            data: $("#user_form").serializeArray()
       });
       this.reset();
       event.preventDefault();
    });

    $("#account_form").submit(function(event){
        $.ajax({
            url:  '/account/add',
            type: 'post',
            data: $("#account_form").serializeArray()
       });
       this.reset();
       event.preventDefault();
    });

});
